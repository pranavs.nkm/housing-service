package com.mail.and.media.validator;

import com.mail.and.media.errorcode.HousingErrorCode;
import com.mail.and.media.exception.HousingRestInputValidationError;
import com.mail.and.media.exception.HousingServiceException;
import com.mail.and.media.i18n.LocaleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;
import java.util.function.Function;

@Component
@AllArgsConstructor
public class RequestHandler {
    private final Validator validator;
    private final LocaleService localeService;

    public <BODY> Mono<ServerResponse> requireValidBody(Function<Mono<BODY>, Mono<ServerResponse>> block, ServerRequest request, Class<BODY> bodyClass) {
        return request.bodyToMono(bodyClass).switchIfEmpty(Mono.error(new HousingServiceException("Please send valid body", HousingErrorCode.HOUSE_BAD_REQUEST_BODY)))
                .flatMap(body -> {
                    Set<ConstraintViolation<BODY>> errors = validator.validate(body);
                    if (errors.isEmpty()) {
                        return block.apply(Mono.just(body));
                    } else {
                        return Mono.error(new HousingRestInputValidationError(localeService.getViolations(request, errors)));
                    }
                });
    }
}
