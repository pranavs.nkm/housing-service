package com.mail.and.media.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class HouseResponseDTO {

    /**
     * Id of the house, unique id given to each house
     */
    private String id;
    /**
     * Name of the house, unique name given to each house
     */
    private String name;
    /**
     * Street Address of the house
     */
    private String addressStreet;
    /**
     * House Number of the house
     */
    private String addressHouseNumber;
    /**
     * City of the house
     */
    private String addressCity;
    /**
     * Area Code of the house
     */
    private String addressAreaCode;
    /**
     * Country of the house
     */
    private String addressCountry;

    /**
     *  house added time with timezone
     */
    private String createdAt;
    /**
     *  house last updated time with timezone
     */
    private String updatedAt;
}
