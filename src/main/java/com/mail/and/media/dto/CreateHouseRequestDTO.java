package com.mail.and.media.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * DTO representing create request for house
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Validated
@ToString
public class CreateHouseRequestDTO {
    /**
     * Name of the house, unique name given to each house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_name")
    private String name;
    /**
     * Street Address of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_street")
    private String addressStreet;
    /**
     * House Number of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_house_no")
    private String addressHouseNumber;
    /**
     * City of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_city")
    private String addressCity;
    /**
     * Area Code of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_area_code")
    private String addressAreaCode;
    /**
     * Country of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_country")
    private String addressCountry;
}
