package com.mail.and.media.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateHouseRequestDTO {

    /**
     * Id of the house, unique id given to each house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_id")
    private String id;
    /**
     * Name of the house, unique name given to each house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_name")
    private String name;
    /**
     * Street Address of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_street")
    private String addressStreet;
    /**
     * House Number of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_house_no")
    private String addressHouseNumber;
    /**
     * City of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_city")
    private String addressCity;
    /**
     * Area Code of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_area_code")
    private String addressAreaCode;
    /**
     * Country of the house
     */
    @NotBlank(message = "error.housing.update.house.dto.missing_address_country")
    private String addressCountry;
}
