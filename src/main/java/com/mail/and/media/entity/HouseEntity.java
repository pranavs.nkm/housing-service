package com.mail.and.media.entity;

import lombok.*;

import java.time.ZonedDateTime;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HouseEntity {
    /**
     * Id of the house, unique id given to each house
     */
    private String id;
    /**
     * Name of the house, unique name given to each house
     */
    private String name;
    /**
     * Address of the house
     */
    private Address address;

    /**
     * house added time with timezone
     */
    private ZonedDateTime createdAt;
    /**
     * house last updated time with timezone
     */
    private ZonedDateTime updatedAt;
}
