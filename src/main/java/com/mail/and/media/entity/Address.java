package com.mail.and.media.entity;

import lombok.*;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    /**
     * Street Address of the house
     */
    private String street;
    /**
     * House Number of the house
     */
    private String houseNumber;
    /**
     * City of the house
     */
    private String city;
    /**
     * Area Code of the house
     */
    private String areaCode;
    /**
     * Country of the house
     */
    private String country;
}
