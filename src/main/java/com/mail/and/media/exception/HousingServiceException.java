package com.mail.and.media.exception;

import com.mail.and.media.errorcode.ErrorCode;
import lombok.Getter;

/**
 * Generic error message representing application error
 */
public class HousingServiceException extends RuntimeException {

    /**
     * Error code representing the error
     */
    @Getter
    private final ErrorCode errorCode;
    /**
     * Error message arguments for generation i18n translated message
     */
    @Getter
    private final Object[] i18nArgs;

    /**
     * @param msg Error message logged
     * @param errorCode {@link ErrorCode} representing exception
     * @param i18nArgs Arguments for i18n translation
     */
    public HousingServiceException(String msg, ErrorCode errorCode, Object... i18nArgs) {
        super(msg);
        this.errorCode = errorCode;
        this.i18nArgs = i18nArgs;
    }
}
