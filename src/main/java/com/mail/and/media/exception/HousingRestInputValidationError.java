package com.mail.and.media.exception;

import com.mail.and.media.errorcode.HousingErrorCode;
import lombok.*;

import java.util.List;


/**
 * {@link HousingRestInputValidationError} is thrown when javax validation error occurs
 */
public class HousingRestInputValidationError extends HousingServiceException {
    @Getter
    private final List<Violation> violations;

    /**
     * @param violations All javax violations with field and corresponding error message
     */
    public HousingRestInputValidationError(List<Violation> violations) {
        super("Error occurred while validating request body", HousingErrorCode.HOUSE_BAD_REQUEST_BODY, new Object[]{});
        this.violations = violations;
    }

    /**
     * Javax violation representation for sending error response
     */
    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Violation {
        /**
         * Field having javax error
         */
        String field;
        /**
         * Error message representing the error field
         */
        String message;
    }
}
