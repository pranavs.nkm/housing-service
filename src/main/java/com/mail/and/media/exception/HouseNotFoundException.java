package com.mail.and.media.exception;

import com.mail.and.media.errorcode.HousingErrorCode;
import lombok.Getter;

/**
 * {@link HouseNotFoundException} is thrown when House with specific id is not found
 */
@Getter
public class HouseNotFoundException extends HousingServiceException {
    private final String id;

    /**
     * @param id representing the id of the house missing
     */
    public HouseNotFoundException(String id) {
        super("House with id: " + id + " not exists", HousingErrorCode.HOUSE_NOT_FOUND, new Object[]{id});
        this.id = id;
    }
}
