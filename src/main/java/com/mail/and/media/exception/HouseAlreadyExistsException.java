package com.mail.and.media.exception;

import com.mail.and.media.errorcode.HousingErrorCode;
import lombok.Getter;

/**
 * When a house with name is found {@link HouseAlreadyExistsException} is thrown
 */
@Getter
public class HouseAlreadyExistsException extends HousingServiceException {
    private final String name;

    /**
     *
     * @param name name of the house which already exists
     */
    public HouseAlreadyExistsException(String name) {
        super("House with name: " + name + " exists", HousingErrorCode.HOUSE_ALREADY_EXISTS, new Object[]{name});
        this.name = name;
    }
}
