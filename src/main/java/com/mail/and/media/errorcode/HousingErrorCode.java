package com.mail.and.media.errorcode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Housing Service Error Codes
 */
@AllArgsConstructor
public enum HousingErrorCode implements ErrorCode {
    HOUSE_NOT_FOUND("ER_HS_01", "error.housing.not.found", HttpStatus.NOT_FOUND),
    HOUSE_ALREADY_EXISTS("ER_HS_02", "error.housing.already.exists", HttpStatus.INTERNAL_SERVER_ERROR),
    HOUSE_BAD_REQUEST_BODY("ER_HS_03", "error.housing.bad.request.body", HttpStatus.BAD_REQUEST);
    /**
     * @see ErrorCode#getCode()
     */
    @Getter
    private String code;
    /**
     * @see ErrorCode#getI18nCode()
     */
    @Getter
    private String i18nCode;
    /**
     * @see ErrorCode#getStatus()
     */
    @Getter
    private HttpStatus status;
}
