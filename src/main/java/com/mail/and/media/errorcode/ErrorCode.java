package com.mail.and.media.errorcode;

import org.springframework.http.HttpStatus;


/**
 * Error code representation of application
 */
public interface ErrorCode {
    /**
     * @return error code for documentation and future reference
     */
    String getCode();

    /**
     * @return Generic message code for loading i18n message
     */
    String getI18nCode();

    /**
     * @return Http Status of rest endpoints on failure
     */
    HttpStatus getStatus();
}
