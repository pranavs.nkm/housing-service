package com.mail.and.media.store;

import com.mail.and.media.entity.HouseEntity;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Scope("singleton")
public class HousingStore {
    private static final Map<String, HouseEntity> HOUSE_ENTITY_MAP = new ConcurrentHashMap<>();

    public HousingStore() {
        HOUSE_ENTITY_MAP.clear();
    }

    public Mono<HouseEntity> add(HouseEntity houseEntity) {
        HOUSE_ENTITY_MAP.put(houseEntity.getId(), houseEntity);
        return Mono.just(houseEntity);
    }


    public Mono<HouseEntity> update(HouseEntity houseEntity) {
        HOUSE_ENTITY_MAP.put(houseEntity.getId(), houseEntity);
        return Mono.just(houseEntity);
    }

    public Mono<Boolean> delete(String id) {
        if (HOUSE_ENTITY_MAP.containsKey(id)) {
            HOUSE_ENTITY_MAP.remove(id);
            return Mono.just(true);
        }
        return Mono.just(false);
    }

    public Flux<HouseEntity> get() {
        return Flux.fromStream(HOUSE_ENTITY_MAP.values().stream());
    }

    public Mono<HouseEntity> get(String id) {
        if (HOUSE_ENTITY_MAP.containsKey(id)) {
            return Mono.just(HOUSE_ENTITY_MAP.get(id));
        }
        return Mono.empty();
    }

    public Mono<HouseEntity> getByName(String name) {
        return HOUSE_ENTITY_MAP.values().stream().filter(houseEntity -> houseEntity.getName().equals(name.trim())).map(houseEntity -> Mono.just(houseEntity)).findFirst().orElse(Mono.empty());
    }
}
