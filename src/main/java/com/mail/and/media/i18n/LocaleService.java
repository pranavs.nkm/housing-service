package com.mail.and.media.i18n;

import com.mail.and.media.exception.HousingRestInputValidationError;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebExchange;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

public interface LocaleService {
    String getMessage(String code, ServerWebExchange exchange, Object[] args);

    <BODY> List<HousingRestInputValidationError.Violation> getViolations(ServerRequest request, Set<ConstraintViolation<BODY>> errors);
}
