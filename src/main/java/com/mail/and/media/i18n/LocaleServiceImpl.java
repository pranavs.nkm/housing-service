package com.mail.and.media.i18n;

import com.mail.and.media.exception.HousingRestInputValidationError;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebExchange;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class LocaleServiceImpl implements LocaleService {
    private final MessageSource messageSource;

    @Override
    public String getMessage(String code, ServerWebExchange exchange, Object... args) {
        return messageSource.getMessage(code, args, exchange.getLocaleContext().getLocale());
    }


    @Override
    public <BODY> List<HousingRestInputValidationError.Violation> getViolations(ServerRequest request, Set<ConstraintViolation<BODY>> errors) {
        List<HousingRestInputValidationError.Violation> violations = errors.stream().map(
                constraintViolation -> {
                    String field = constraintViolation.getPropertyPath().toString();
                    String message = getMessage(constraintViolation.getMessageTemplate(), request.exchange(), null);
                    return HousingRestInputValidationError.Violation.builder().field(field).message(message).build();
                }
        ).toList();
        return violations;
    }
}
