package com.mail.and.media.service;

import com.mail.and.media.dto.CreateHouseRequestDTO;
import com.mail.and.media.dto.HouseResponseDTO;
import com.mail.and.media.dto.UpdateHouseRequestDTO;
import com.mail.and.media.entity.HouseEntity;
import com.mail.and.media.exception.HouseAlreadyExistsException;
import com.mail.and.media.exception.HouseNotFoundException;
import com.mail.and.media.repository.HousingRepository;
import com.mail.and.media.util.DateUtil;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;

@Service
@AllArgsConstructor
public class HousingServiceImpl implements HousingService {

    private final ModelMapper modelMapper;
    private final DateUtil dateUtil;
    private final HousingRepository housingRepository;

    @Override
    public Mono<HouseResponseDTO> getHouseById(String id) {
        return housingRepository.getHouseById(id).map(this::toHouseResponseDTO).switchIfEmpty(Mono.defer(() -> Mono.error(new HouseNotFoundException(id))));
    }

    @Override
    public Flux<HouseResponseDTO> getAllHouses() {
        return housingRepository.getAllHouses().map(this::toHouseResponseDTO);
    }

    @Override
    public Mono<HouseResponseDTO> addHouse(CreateHouseRequestDTO createHouseRequestDTO) {
        return housingRepository
                .getHouseByName(createHouseRequestDTO.getName())
                .defaultIfEmpty(new HouseEntity())
                .flatMap(current -> {
                    if (current.getId() != null) {
                        return Mono.error(new HouseAlreadyExistsException(createHouseRequestDTO.getName()));
                    }
                    return housingRepository.addHouse(toHouseEntity(createHouseRequestDTO)).map(this::toHouseResponseDTO);
                });
    }

    @Override
    public Mono<HouseResponseDTO> updateHouse(UpdateHouseRequestDTO updateHouseRequestDTO) {
        return housingRepository
                .getHouseById(updateHouseRequestDTO.getId())
                .defaultIfEmpty(new HouseEntity())
                .flatMap(current -> {
                    if (current.getId() == null) {
                        return Mono.error(new HouseNotFoundException(updateHouseRequestDTO.getId()));
                    }
                    return housingRepository.getHouseByName(updateHouseRequestDTO.getName());
                }).flatMap(current -> {
                    if (!current.getId().equals(updateHouseRequestDTO.getId())) {
                        return Mono.error(new HouseAlreadyExistsException(updateHouseRequestDTO.getName()));
                    }
                    return housingRepository.updateHouse(toHouseEntity(updateHouseRequestDTO)).map(this::toHouseResponseDTO);
                });
    }

    @Override
    public Mono<Boolean> deleteHouseById(String id) {
        return housingRepository
                .getHouseById(id)
                .defaultIfEmpty(new HouseEntity())
                .flatMap(current -> {
                    if (current.getId() == null) {
                        return Mono.error(new HouseNotFoundException(id));
                    }
                    return housingRepository.deleteHouseById(id);
                });

    }


//  Interview live coding question
//
//    @Override
//    public Mono<Map<String,String>> compareHousesById(String id1, String id2) {
//        return housingRepository
//                .getHouseById(id1)
//                .switchIfEmpty(Mono.error(new RuntimeException()))
//                .flatMap(house1 -> housingRepository
//                        .getHouseById(id2)
//                        .switchIfEmpty(Mono.error(new RuntimeException()))
//                        .map(house2 -> {
//                            Map<String,String> result=new HashMap<>();
//                            if(!house1.getName().equals(house2.getName())){
//                                result.put("name","Not matching");
//                            }
//                            return result;
//                        }));
//    }
//
//    public Flux<HouseEntity> getAllHousesByLAstAddedOrder(){
//        return housingRepository.getAllHouses().sort((o1, o2) -> o2.getId().compareTo(o1.getId()));
//    }

    @Override
    public HouseEntity toHouseEntity(CreateHouseRequestDTO createHouseRequestDTO) {
        HouseEntity houseEntity = modelMapper.map(createHouseRequestDTO, HouseEntity.class);
        houseEntity.setCreatedAt(dateUtil.utcDateFromCurrentDate());
        houseEntity.setId(UUID.randomUUID().toString());
        return houseEntity;
    }

    @Override
    public HouseEntity toHouseEntity(UpdateHouseRequestDTO updateHouseRequestDTO) {

        HouseEntity houseEntity = modelMapper.map(updateHouseRequestDTO, HouseEntity.class);
        houseEntity.setCreatedAt(housingRepository
                .getHouseById(updateHouseRequestDTO.getId()).block().getCreatedAt());
        houseEntity.setUpdatedAt(dateUtil.utcDateFromCurrentDate());
        return houseEntity;
    }

    @Override
    public HouseResponseDTO toHouseResponseDTO(HouseEntity houseEntity) {
        return modelMapper.map(houseEntity, HouseResponseDTO.class);
    }
}
