package com.mail.and.media.service;

import com.mail.and.media.dto.CreateHouseRequestDTO;
import com.mail.and.media.dto.HouseResponseDTO;
import com.mail.and.media.dto.UpdateHouseRequestDTO;
import com.mail.and.media.entity.HouseEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface HousingService {

    HouseEntity toHouseEntity(CreateHouseRequestDTO createHouseRequestDTO);

    HouseEntity toHouseEntity(UpdateHouseRequestDTO updateHouseRequestDTO);

    HouseResponseDTO toHouseResponseDTO(HouseEntity houseEntity);

    Mono<HouseResponseDTO> getHouseById(String id);

    Flux<HouseResponseDTO> getAllHouses();

    Mono<HouseResponseDTO> addHouse(CreateHouseRequestDTO createHouseRequestDTO);

    Mono<HouseResponseDTO> updateHouse(UpdateHouseRequestDTO updateHouseRequestDTO);

    Mono<Boolean> deleteHouseById(String id);

    //  Interview live coding question
//
//    Mono<Boolean> compareHousesById(String id1,String id2);
}
