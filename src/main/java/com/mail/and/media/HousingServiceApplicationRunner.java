package com.mail.and.media;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Housing Service Application Runner
 */
@SpringBootApplication
public class HousingServiceApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(HousingServiceApplicationRunner.class, args);
    }
}
