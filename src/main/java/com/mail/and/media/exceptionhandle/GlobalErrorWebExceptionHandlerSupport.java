package com.mail.and.media.exceptionhandle;

import com.mail.and.media.errorcode.ErrorCode;
import com.mail.and.media.exception.HousingRestInputValidationError;
import com.mail.and.media.exception.HousingServiceException;
import com.mail.and.media.i18n.LocaleService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.Locale;
import java.util.Map;


/**
 * addition support class for handling exception
 */
@Component
@AllArgsConstructor
public class GlobalErrorWebExceptionHandlerSupport {

    /**
     * Locale service provides support to identify locale from {@link ServerRequest}, also to properly translate i18n message code
     */
    private final LocaleService localeService;

    /**
     * @param request Represents a server-side HTTP request, as handled by a HandlerFunction.
     * @param error Exception to be handled
     * @param errorPropertiesMap error properties
     * @return Error response with error representation
     */
    public Mono<ServerResponse> renderErrorResponse(
            ServerRequest request, Throwable error, Map<String, Object> errorPropertiesMap) {
        if (error instanceof HousingServiceException) {
            Locale locale = request.exchange().getLocaleContext().getLocale();
            HousingServiceException houseNotFoundException = (HousingServiceException) error;
            ErrorCode errorCode = houseNotFoundException.getErrorCode();
            errorPropertiesMap.put("error", errorCode.getCode());
            errorPropertiesMap.put("message", localeService.getMessage(errorCode.getI18nCode(), request.exchange(), houseNotFoundException.getI18nArgs()));
            errorPropertiesMap.put("status", errorCode.getStatus().value());
            if (error instanceof HousingRestInputValidationError) {
                errorPropertiesMap.put("violations", ((HousingRestInputValidationError) error).getViolations());
            }
        }
        return ServerResponse.status((Integer) errorPropertiesMap.get("status"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(errorPropertiesMap));
    }
}
