package com.mail.and.media.exceptionhandle;

import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

/**
 * Global exception handler
 */
@Component
@Order(-2)
public class GlobalErrorWebExceptionHandler extends AbstractErrorWebExceptionHandler {

    /**
     * @param errorAttributes Provides access to error attributes which can be logged or presented to the user
     * @param webProperties Configuration properties for general web concerns.
     * @param applicationContext Central interface to provide configuration for an application. This is read-only while the application is running, but may be reloaded if the implementation supports this.
     * @param configurer Extension of CodecConfigurer for HTTP message reader and writer options relevant on the server side.
     * @param globalErrorWebExceptionHandlerSupport addition support class for handling exception
     */
    public GlobalErrorWebExceptionHandler(ErrorAttributes errorAttributes, WebProperties webProperties,
                                          ApplicationContext applicationContext, ServerCodecConfigurer configurer, GlobalErrorWebExceptionHandlerSupport globalErrorWebExceptionHandlerSupport) {
        super(errorAttributes, webProperties.getResources(), applicationContext);
        this.globalErrorWebExceptionHandlerSupport = globalErrorWebExceptionHandlerSupport;
        this.setMessageWriters(configurer.getWriters());
    }

    /**
     * {@link GlobalErrorWebExceptionHandlerSupport} addition support class for handling exception
     */
    private final GlobalErrorWebExceptionHandlerSupport globalErrorWebExceptionHandlerSupport;

    /**
     * @param errorAttributes the {@code ErrorAttributes} instance to use to extract error
     *                        information
     * @return Global Error handler wrapped router function
     */
    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(
            ErrorAttributes errorAttributes) {

        return RouterFunctions.route(
                RequestPredicates.all(), this::renderErrorResponse);
    }

    /**
     * @param request Represents a server-side HTTP request, as handled by a HandlerFunction.
     * @return Global Error response error renderer
     */
    private Mono<ServerResponse> renderErrorResponse(
            ServerRequest request) {
        return globalErrorWebExceptionHandlerSupport.renderErrorResponse(request, getError(request), getErrorAttributes(request, ErrorAttributeOptions.defaults()));
    }
}