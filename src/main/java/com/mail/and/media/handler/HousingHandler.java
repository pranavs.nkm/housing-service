package com.mail.and.media.handler;

import com.mail.and.media.dto.CreateHouseRequestDTO;
import com.mail.and.media.dto.HouseResponseDTO;
import com.mail.and.media.dto.UpdateHouseRequestDTO;
import com.mail.and.media.exception.HouseNotFoundException;
import com.mail.and.media.i18n.LocaleService;
import com.mail.and.media.service.HousingService;
import com.mail.and.media.validator.RequestHandler;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;

/**
 * Housing DTO Handler routing functions for web api
 */
@Component
@AllArgsConstructor
public class HousingHandler {
    /**
     * Generic hadnler class to handle common functionality such as javax validation
     */
    private RequestHandler requestHandler;
    /**
     * Housing service object to handle house
     */
    private final HousingService housingService;

    /**
     * @param serverRequest Represents a server-side HTTP request, as handled by a HandlerFunction.
     * @return returns all houses as flux stream
     */
    public Mono<ServerResponse> getAllHouses(ServerRequest serverRequest) {
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                .body(housingService.getAllHouses()
                        , HouseResponseDTO.class);
    }

    /**
     * @param serverRequest Represents a server-side HTTP request, as handled by a HandlerFunction.
     * @return returns response with specific house as mono
     */
    public Mono<ServerResponse> getHouseById(ServerRequest serverRequest) {
        String id = serverRequest.pathVariable("id");
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                .body(housingService.getHouseById(id), HouseResponseDTO.class);

    }

    /**
     * @param serverRequest Represents a server-side HTTP request, as handled by a HandlerFunction.
     * @return returns Response 201 when house is added, Error is thrown otherwise
     */
    public Mono<ServerResponse> addHouse(ServerRequest serverRequest) {
        return requestHandler.requireValidBody(body ->
        {
            return body.flatMap(createHouseRequestDTO -> housingService.addHouse(createHouseRequestDTO))
                    .flatMap(houseResponseDTO -> ServerResponse.created(URI.create("/houses/" + houseResponseDTO.getId()))
                            .contentType(MediaType.APPLICATION_JSON)
                            .build());
        }, serverRequest, CreateHouseRequestDTO.class);
    }


    /**
     * @param serverRequest Represents a server-side HTTP request, as handled by a HandlerFunction.
     * @return returns Response 200 when house is updated
     */
    public Mono<ServerResponse> updateHouse(ServerRequest serverRequest) {

        return requestHandler.requireValidBody(body ->
                body.flatMap(updateHouseRequestDTO -> housingService.updateHouse(updateHouseRequestDTO))
                .flatMap(houseResponseDTO -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(houseResponseDTO))), serverRequest, UpdateHouseRequestDTO.class);
    }

    /**
     * @param serverRequest Represents a server-side HTTP request, as handled by a HandlerFunction.
     * @return returns Response 204 when house is deleted
     */
    public Mono<ServerResponse> deleteHouseById(ServerRequest serverRequest) {

        String id = serverRequest.pathVariable("id");
        return housingService.deleteHouseById(id)
                .flatMap(val -> {
                    if (val) {
                        return ServerResponse.noContent().build();
                    }
                    return Mono.error(new HouseNotFoundException(id));
                });
    }
}
