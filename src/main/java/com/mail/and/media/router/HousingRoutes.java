package com.mail.and.media.router;

import com.mail.and.media.handler.HousingHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class HousingRoutes {
    @Bean
    public RouterFunction<ServerResponse> root(HousingHandler housingHandler) {
        return RouterFunctions.route()
                .GET("/houses", RequestPredicates.accept(MediaType.TEXT_PLAIN), housingHandler::getAllHouses)
                .GET("/houses/{id}", RequestPredicates.accept(MediaType.TEXT_PLAIN), housingHandler::getHouseById)
                .POST("/houses", RequestPredicates.contentType(MediaType.APPLICATION_JSON), housingHandler::addHouse)
                .PUT("/houses", RequestPredicates.contentType(MediaType.APPLICATION_JSON), housingHandler::updateHouse)
                .DELETE("/houses/{id}", RequestPredicates.accept(MediaType.TEXT_PLAIN), housingHandler::deleteHouseById)
                .build();
    }
}
