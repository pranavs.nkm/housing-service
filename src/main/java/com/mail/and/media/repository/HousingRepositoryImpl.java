package com.mail.and.media.repository;

import com.mail.and.media.entity.HouseEntity;
import com.mail.and.media.store.HousingStore;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
@AllArgsConstructor
public class HousingRepositoryImpl implements HousingRepository {
    private final HousingStore housingStore;

    @Override
    public Flux<HouseEntity> getAllHouses() {
        return housingStore.get();
    }

    @Override
    public Mono<HouseEntity> addHouse(HouseEntity houseEntity) {
        return housingStore.add(houseEntity);
    }

    @Override
    public Mono<HouseEntity> updateHouse(HouseEntity houseEntity) {
        return housingStore.update(houseEntity);
    }

    @Override
    public Mono<Boolean> deleteHouseById(String id) {
        return housingStore.delete(id);
    }

    @Override
    public Mono<HouseEntity> getHouseById(String id) {
        return housingStore.get(id);
    }

    @Override
    public Mono<HouseEntity> getHouseByName(String name) {
        return housingStore.getByName(name);
    }
}
