package com.mail.and.media.repository;

import com.mail.and.media.entity.HouseEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface HousingRepository {

    Flux<HouseEntity> getAllHouses();

    Mono<HouseEntity> addHouse(HouseEntity toHouseEntity);

    Mono<HouseEntity> updateHouse(HouseEntity toHouseEntity);

    Mono<Boolean> deleteHouseById(String id);

    Mono<HouseEntity> getHouseById(String id);

    Mono<HouseEntity> getHouseByName(String name);
}
