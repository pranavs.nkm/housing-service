package com.mail.and.media.util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Component
public class DateUtil {
    public ZonedDateTime utcDateFromCurrentDate() {
        return utcDateFromDate(LocalDateTime.now().atZone(ZoneId.systemDefault()));
    }

    public ZonedDateTime utcDateFromDate(ZonedDateTime zdt) {
        ZonedDateTime utcZoned = zdt.withZoneSameInstant(ZoneId.of("UTC"));
        return utcZoned;
    }
}
