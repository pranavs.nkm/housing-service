package com.mail.and.media.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;

class DateUtilTest {

    @Test
    void utcDateFromCurrentDate() {
        DateUtil dateUtil = new DateUtil();
        ZonedDateTime zonedDateTime = dateUtil.utcDateFromCurrentDate();
        Assertions.assertNotNull(zonedDateTime);
        Assertions.assertEquals(ZoneId.of("UTC"), zonedDateTime.getZone());
    }

    @Test
    void utcDateFromDate() {
        DateUtil dateUtil = new DateUtil();
        ZonedDateTime zdt = ZonedDateTime.now();
        ZonedDateTime zonedDateTime = dateUtil.utcDateFromDate(zdt);
        Assertions.assertNotNull(zonedDateTime);
        Assertions.assertEquals(ZoneId.of("UTC"), zonedDateTime.getZone());
        ZonedDateTime dateTime = zdt.withZoneSameInstant(ZoneId.systemDefault());
        Assertions.assertEquals(dateTime, zdt);
    }
}