package com.mail.and.media.router;

import com.mail.and.media.exception.HousingRestInputValidationError;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
    private Long timestamp;
    private String path;
    private Integer status;
    private String error;
    private String requestId;
    private String message;
    private List<HousingRestInputValidationError.Violation> violations;
}
