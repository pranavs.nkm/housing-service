package com.mail.and.media.router;

import com.mail.and.media.config.AppConfig;
import com.mail.and.media.dto.CreateHouseRequestDTO;
import com.mail.and.media.dto.HouseResponseDTO;
import com.mail.and.media.dto.UpdateHouseRequestDTO;
import com.mail.and.media.exception.HouseAlreadyExistsException;
import com.mail.and.media.exception.HouseNotFoundException;
import com.mail.and.media.exceptionhandle.GlobalErrorWebExceptionHandler;
import com.mail.and.media.exceptionhandle.GlobalErrorWebExceptionHandlerSupport;
import com.mail.and.media.handler.HousingHandler;
import com.mail.and.media.i18n.LocaleServiceImpl;
import com.mail.and.media.service.HousingService;
import com.mail.and.media.validator.RequestHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = {HousingRoutes.class, HousingHandler.class, LocaleServiceImpl.class, AppConfig.class, RequestHandler.class, GlobalErrorWebExceptionHandler.class, GlobalErrorWebExceptionHandlerSupport.class})
@WebFluxTest
class HousingRoutesTest {

    @Autowired
    private ApplicationContext context;

    @MockBean
    private HousingService housingService;

    private WebTestClient webTestClient;

    @BeforeEach
    public void setUp() {
        webTestClient = WebTestClient.bindToApplicationContext(context).build();
    }

    @Test
    public void getAllHouse() {
        Mockito.when(housingService.getAllHouses()).thenReturn(Flux.fromIterable(Collections.EMPTY_LIST));
        webTestClient.get().uri("/houses").exchange().expectStatus().isOk().expectBodyList(HouseResponseDTO.class).value(houses -> {
            assertEquals(0, houses.size());
        });
    }


    @Test
    public void getAllHouseWithData() {
        final int totalUsers = 4;
        List<HouseResponseDTO> data = IntStream.range(0, totalUsers).boxed().map(i -> HouseResponseDTO.builder().id("dummy-id-" + i).name("dummy-name-" + i).addressAreaCode("dummy-area-code-" + i).addressCity("dummy-city-" + i).addressHouseNumber("dummy-house-number-" + i).addressStreet("dummy-street-" + i).addressCountry("dummy-country-" + i).createdAt(ZonedDateTime.now().toString()).updatedAt(ZonedDateTime.now().toString()).build()).collect(Collectors.toList());
        Mockito.when(housingService.getAllHouses()).thenReturn(Flux.fromIterable(data));
        webTestClient.get().uri("/houses").exchange().expectStatus().isOk().expectBodyList(HouseResponseDTO.class).value(houses -> {
            assertEquals(houses.size(), totalUsers);
            for (int i = 0; i < totalUsers; i++) {
                HouseResponseDTO houseResponseDTO = houses.get(i);
                Assertions.assertEquals("dummy-id-" + i, houseResponseDTO.getId());
                Assertions.assertEquals("dummy-name-" + i, houseResponseDTO.getName());
                Assertions.assertEquals("dummy-house-number-" + i, houseResponseDTO.getAddressHouseNumber());
                Assertions.assertEquals("dummy-city-" + i, houseResponseDTO.getAddressCity());
                Assertions.assertEquals("dummy-area-code-" + i, houseResponseDTO.getAddressAreaCode());
                Assertions.assertEquals("dummy-street-" + i, houseResponseDTO.getAddressStreet());
                Assertions.assertEquals("dummy-country-" + i, houseResponseDTO.getAddressCountry());
            }
        });
    }

    @Test
    public void getHouseById() {
        HouseResponseDTO origHouseResponseDto = HouseResponseDTO.builder().id("id").addressCity("addressCity").addressAreaCode("addressAreaCode").addressHouseNumber("addressHouseNumber").addressCountry("addressCountry").name("name").addressStreet("addressStreet").updatedAt(ZonedDateTime.now().toString()).createdAt(ZonedDateTime.now().toString()).build();
        Mockito.when(housingService.getHouseById("id")).thenReturn(Mono.just(origHouseResponseDto));
        webTestClient.get().uri("/houses/id").exchange().expectStatus().isOk().expectBody(HouseResponseDTO.class).value(houseResponseDTO -> {
            Assertions.assertEquals(origHouseResponseDto, houseResponseDTO);
        });
    }

    @Test
    public void getHouseByIdWhenHousNotExist() {
        Mockito.when(housingService.getHouseById("id")).thenReturn(Mono.error(new HouseNotFoundException("id")));
        webTestClient.get().uri("/houses/id").exchange().expectStatus().isNotFound().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses/id", error.getPath());
            Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), error.getStatus());
            Assertions.assertEquals("ER_HS_01", error.getError());
            Assertions.assertEquals("House with id: id is not found", error.getMessage());
        });
    }

    @Test
    public void addHouseWithoutContentType() {
        webTestClient.post().uri("/houses").exchange().expectStatus().isNotFound().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses", error.getPath());
            Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), error.getStatus());
            Assertions.assertEquals("Not Found", error.getError());
        });
    }

    @Test
    public void addHouseWithoutBody() {
        webTestClient.post().uri("/houses").header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).exchange().expectStatus().isBadRequest().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses", error.getPath());
            Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), error.getStatus());
            Assertions.assertEquals("ER_HS_03", error.getError());
            Assertions.assertEquals("Bad Request body, please correct the violations", error.getMessage());
        });
    }

    @Test
    public void addHouseWithMissingAllFields() {
        webTestClient.post().uri("/houses").bodyValue(Collections.emptyMap()).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).exchange().expectStatus().isBadRequest().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses", error.getPath());
            Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), error.getStatus());
            Assertions.assertEquals("ER_HS_03", error.getError());
            Assertions.assertEquals("Bad Request body, please correct the violations", error.getMessage());
            Assertions.assertNotNull(error.getViolations());
            Assertions.assertEquals(6, error.getViolations().size());
        });
    }

    @Test
    public void addHouseWithMissingSomeFields() {
        webTestClient.post().uri("/houses").bodyValue(CreateHouseRequestDTO.builder().addressAreaCode("Test").addressAreaCode("test").build()).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).exchange().expectStatus().isBadRequest().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses", error.getPath());
            Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), error.getStatus());
            Assertions.assertEquals("ER_HS_03", error.getError());
            Assertions.assertEquals("Bad Request body, please correct the violations", error.getMessage());
            Assertions.assertNotNull(error.getViolations());
            Assertions.assertEquals(5, error.getViolations().size());
        });
    }


    @Test
    public void addHouse() {
        CreateHouseRequestDTO houseDto = CreateHouseRequestDTO.builder().addressCity("addressCity").addressAreaCode("addressAreaCode").addressHouseNumber("addressHouseNumber").addressCountry("addressCountry").name("name").addressStreet("addressStreet").build();

        HouseResponseDTO origHouseResponseDto = HouseResponseDTO.builder().id("id").addressCity("addressCity").addressAreaCode("addressAreaCode").addressHouseNumber("addressHouseNumber").addressCountry("addressCountry").name("name").addressStreet("addressStreet").updatedAt(ZonedDateTime.now().toString()).createdAt(ZonedDateTime.now().toString()).build();
        Mockito.when(housingService.addHouse(houseDto)).thenReturn(Mono.just(origHouseResponseDto));
        webTestClient.post().uri("/houses").bodyValue(houseDto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).exchange().expectStatus().isCreated().expectBody().isEmpty();
    }


    @Test
    public void addHouseWithNameExists() {
        CreateHouseRequestDTO houseDto = CreateHouseRequestDTO.builder().addressCity("addressCity").addressAreaCode("addressAreaCode").addressHouseNumber("addressHouseNumber").addressCountry("addressCountry").name("name").addressStreet("addressStreet").build();

        Mockito.when(housingService.addHouse(houseDto)).thenReturn(Mono.error(new HouseAlreadyExistsException("id")));
        webTestClient.post().uri("/houses").bodyValue(houseDto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).exchange().expectStatus().is5xxServerError().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses", error.getPath());
            Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), error.getStatus());
            Assertions.assertEquals("ER_HS_02", error.getError());
            Assertions.assertEquals("House with name: id exists", error.getMessage());
        });
    }

    @Test
    public void updateHouseWithMissingAllFields() {
        webTestClient.put().uri("/houses").bodyValue(Collections.emptyMap()).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).exchange().expectStatus().isBadRequest().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses", error.getPath());
            Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), error.getStatus());
            Assertions.assertEquals("ER_HS_03", error.getError());
            Assertions.assertEquals("Bad Request body, please correct the violations", error.getMessage());
            Assertions.assertNotNull(error.getViolations());
            Assertions.assertEquals(7, error.getViolations().size());
        });
    }

    @Test
    public void updateHouseWithMissingSomeFields() {
        webTestClient.put().uri("/houses").bodyValue(CreateHouseRequestDTO.builder().addressAreaCode("Test").addressAreaCode("test").build()).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).exchange().expectStatus().isBadRequest().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses", error.getPath());
            Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), error.getStatus());
            Assertions.assertEquals("ER_HS_03", error.getError());
            Assertions.assertEquals("Bad Request body, please correct the violations", error.getMessage());
            Assertions.assertNotNull(error.getViolations());
            Assertions.assertEquals(6, error.getViolations().size());
        });
    }


    @Test
    public void updateHouse() {
        UpdateHouseRequestDTO houseDto = UpdateHouseRequestDTO.builder().id("id").addressCity("addressCity").addressAreaCode("addressAreaCode").addressHouseNumber("addressHouseNumber").addressCountry("addressCountry").name("name").addressStreet("addressStreet").build();

        final HouseResponseDTO origHouseResponseDto = HouseResponseDTO.builder().id("id").addressCity("addressCity").addressAreaCode("addressAreaCode").addressHouseNumber("addressHouseNumber").addressCountry("addressCountry").name("name").addressStreet("addressStreet").updatedAt(ZonedDateTime.now().toString()).createdAt(ZonedDateTime.now().toString()).build();

        Mockito.when(housingService.updateHouse(houseDto)).thenReturn(Mono.just(origHouseResponseDto));
        webTestClient.put().uri("/houses").bodyValue(houseDto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).exchange().expectStatus().isOk().expectBody(HouseResponseDTO.class).value(houseResponseDTO -> {
            Assertions.assertEquals(origHouseResponseDto, houseResponseDTO);
        });
    }


    @Test
    public void deleteHouse() {
        Mockito.when(housingService.deleteHouseById("id")).thenReturn(Mono.just(Boolean.TRUE));
        webTestClient.delete().uri("/houses/id").exchange().expectStatus().isNoContent().expectBody().isEmpty();
    }


    @Test
    public void deleteHouseWhichDoesntExists() {
        Mockito.when(housingService.deleteHouseById("id")).thenReturn(Mono.just(Boolean.FALSE));
        webTestClient.delete().uri("/houses/id").exchange().expectStatus().isNotFound().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses/id", error.getPath());
            Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), error.getStatus());
            Assertions.assertEquals("ER_HS_01", error.getError());
            Assertions.assertEquals("House with id: id is not found", error.getMessage());
        });
    }

    @Test
    public void deleteHouseWhichDoesntExistsWithError() {
        Mockito.when(housingService.deleteHouseById("id")).thenReturn(Mono.error(new HouseNotFoundException("id")));
        webTestClient.delete().uri("/houses/id").exchange().expectStatus().isNotFound().expectBody(ErrorResponse.class).value(error -> {
            Assertions.assertNotNull(error.getTimestamp());
            Assertions.assertNotNull(error.getRequestId());
            Assertions.assertEquals("/houses/id", error.getPath());
            Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), error.getStatus());
            Assertions.assertEquals("ER_HS_01", error.getError());
            Assertions.assertEquals("House with id: id is not found", error.getMessage());
        });
    }
}