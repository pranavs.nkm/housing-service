package com.mail.and.media.validator;

import com.mail.and.media.dto.CreateHouseRequestDTO;
import com.mail.and.media.exception.HousingRestInputValidationError;
import com.mail.and.media.exception.HousingServiceException;
import com.mail.and.media.i18n.LocaleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class RequestHandlerTest {

    @Mock
    private Validator validator;
    @Mock
    private LocaleService localeService;
    @Mock
    private ServerRequest request;
    private RequestHandler requestHandler;

    private static <BODY> Mono<ServerResponse> apply(Mono<BODY> result) {
        return result.flatMap(data -> ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue(data)));
    }


    @BeforeEach
    public void init() {
        this.requestHandler = new RequestHandler(validator, localeService);
    }

    @Test
    void requireValidBodyOnEmptyBody() {
        Mockito.when(request.bodyToMono(Map.class)).thenReturn(Mono.empty());
        Mono<ServerResponse> serverResponseMono = this.requestHandler.requireValidBody(RequestHandlerTest::apply, request, Map.class);
        StepVerifier.create(serverResponseMono)
                .expectError(HousingServiceException.class)
                .verify();
    }

    @Test
    void requireValidBodyOnValidationError() {
        CreateHouseRequestDTO dto = CreateHouseRequestDTO.builder().build();
        Mockito.when(request.bodyToMono(CreateHouseRequestDTO.class)).thenReturn(Mono.just(dto));
        Set<ConstraintViolation<CreateHouseRequestDTO>> errors = new HashSet<>();
        errors.add(Mockito.mock(ConstraintViolation.class));
        Mockito.when(validator.validate(dto)).thenReturn(errors);
        Mono<ServerResponse> serverResponseMono = this.requestHandler.requireValidBody(RequestHandlerTest::apply, request, CreateHouseRequestDTO.class);
        StepVerifier.create(serverResponseMono)
                .expectError(HousingRestInputValidationError.class)
                .verify();
    }

    @Test
    void requireValidBodyOnValidationNoError() {
        CreateHouseRequestDTO dto = CreateHouseRequestDTO.builder().build();
        Mockito.when(request.bodyToMono(CreateHouseRequestDTO.class)).thenReturn(Mono.just(dto));
        Mono<ServerResponse> serverResponseMono = this.requestHandler.requireValidBody(RequestHandlerTest::apply, request, CreateHouseRequestDTO.class);
        ServerResponse serverResponse = serverResponseMono.block();
        assertNotNull(serverResponse);
    }
}