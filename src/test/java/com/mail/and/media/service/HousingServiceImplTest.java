package com.mail.and.media.service;

import com.mail.and.media.dto.CreateHouseRequestDTO;
import com.mail.and.media.dto.HouseResponseDTO;
import com.mail.and.media.dto.UpdateHouseRequestDTO;
import com.mail.and.media.entity.Address;
import com.mail.and.media.entity.HouseEntity;
import com.mail.and.media.exception.HouseAlreadyExistsException;
import com.mail.and.media.exception.HouseNotFoundException;
import com.mail.and.media.repository.HousingRepository;
import com.mail.and.media.util.DateUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
class HousingServiceImplTest {

    private HousingService housingService;
    private ModelMapper modelMapper;
    @Mock
    private DateUtil dateUtil;
    @Mock
    HousingRepository housingRepository;

    @BeforeEach
    public void init() {
        this.modelMapper = new ModelMapper();
        this.housingService = new HousingServiceImpl(modelMapper, dateUtil, housingRepository);
    }

    @Test
    public void getHouseByIdWhenAbsent() {
        Mockito.when(housingRepository.getHouseById("id")).thenReturn(Mono.empty());
        Mono<HouseResponseDTO> house = housingService.getHouseById("id");
        StepVerifier.create(house).expectError(HouseNotFoundException.class).verify();
    }

    @Test
    public void getHouseByIdWhenPresent() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .build();
        Mockito.when(housingRepository.getHouseById("id")).thenReturn(Mono.just(houseEntity));
        Mono<HouseResponseDTO> house = housingService.getHouseById("id");
        HouseResponseDTO houseResponseDto = house.block();
        Assertions.assertNotNull(houseResponseDto);
        Assertions.assertEquals(houseEntity.getId(), houseResponseDto.getId());
        Assertions.assertEquals(houseEntity.getName(), houseResponseDto.getName());
        Assertions.assertEquals(houseEntity.getAddress().getHouseNumber(), houseResponseDto.getAddressHouseNumber());
        Assertions.assertEquals(houseEntity.getAddress().getCity(), houseResponseDto.getAddressCity());
        Assertions.assertEquals(houseEntity.getAddress().getAreaCode(), houseResponseDto.getAddressAreaCode());
        Assertions.assertEquals(houseEntity.getAddress().getCountry(), houseResponseDto.getAddressCountry());
    }

    @Test
    public void getAllHousesWhenEmpty() {
        Mockito.when(housingRepository.getAllHouses()).thenReturn(Flux.empty());
        Flux<HouseResponseDTO> houses = housingService.getAllHouses();
        List<HouseResponseDTO> housesCollection = houses.collectList().block();
        Assertions.assertEquals(0, housesCollection.size());
    }

    @Test
    public void getAllHousesWhenHaveContent() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .build();

        Mockito.when(housingRepository.getAllHouses()).thenReturn(Flux.just(houseEntity));
        Flux<HouseResponseDTO> houses = housingService.getAllHouses();
        List<HouseResponseDTO> housesCollection = houses.collectList().block();
        Assertions.assertEquals(1, housesCollection.size());

        HouseResponseDTO houseResponseDTO = housesCollection.get(0);
        Assertions.assertNotNull(houseResponseDTO);
        Assertions.assertEquals(houseEntity.getId(), houseResponseDTO.getId());
        Assertions.assertEquals(houseEntity.getName(), houseResponseDTO.getName());
        Assertions.assertEquals(houseEntity.getAddress().getHouseNumber(), houseResponseDTO.getAddressHouseNumber());
        Assertions.assertEquals(houseEntity.getAddress().getCity(), houseResponseDTO.getAddressCity());
        Assertions.assertEquals(houseEntity.getAddress().getAreaCode(), houseResponseDTO.getAddressAreaCode());
        Assertions.assertEquals(houseEntity.getAddress().getCountry(), houseResponseDTO.getAddressCountry());
    }

    @Test
    public void addHouseWhenHouseExists() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .build();

        CreateHouseRequestDTO houseResponseDTO = CreateHouseRequestDTO
                .builder()
                .name("name")
                .addressAreaCode("areaCode")
                .addressHouseNumber("houseNumber")
                .addressCity("city")
                .addressCountry("country")
                .addressStreet("street")
                .build();

        Mockito.when(housingRepository.getHouseByName(houseEntity.getName())).thenReturn(Mono.just(houseEntity));
        Mono<HouseResponseDTO> houseResponseDTOMono = housingService.addHouse(houseResponseDTO);
        StepVerifier.create(houseResponseDTOMono)
                .expectError(HouseAlreadyExistsException.class).verify();
    }

    @Test
    public void addHouseWhenNotExists() {
        try (MockedStatic<UUID> utilities = Mockito.mockStatic(UUID.class)) {
            UUID uuid = Mockito.mock(UUID.class);
            Mockito.when(uuid.toString()).thenReturn("3d15133c-4a72-4b22-8a25-80364fa7fc5e");
            utilities.when(UUID::randomUUID).thenReturn(uuid);

            ZonedDateTime zonedDateTime = ZonedDateTime.now();
            HouseEntity houseEntity = HouseEntity
                    .builder()
                    .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e")
                    .name("name")
                    .address(
                            Address
                                    .builder()
                                    .areaCode("areaCode")
                                    .city("city")
                                    .houseNumber("houseNumber")
                                    .country("country")
                                    .street("street")
                                    .build()
                    )
                    .createdAt(zonedDateTime)
                    .build();
            CreateHouseRequestDTO houseResponseDTO = CreateHouseRequestDTO
                    .builder()
                    .name("name")
                    .addressAreaCode("areaCode")
                    .addressHouseNumber("houseNumber")
                    .addressCity("city")
                    .addressCountry("country")
                    .addressStreet("street")
                    .build();

            Mockito.when(housingRepository.getHouseByName(houseResponseDTO.getName())).thenReturn(Mono.empty());
            Mockito.when(housingRepository.addHouse(houseEntity)).thenReturn(Mono.just(houseEntity));
            Mockito.when(dateUtil.utcDateFromCurrentDate()).thenReturn(zonedDateTime);
            Mono<HouseResponseDTO> houseResponseDTOMono = housingService.addHouse(houseResponseDTO);
            HouseResponseDTO responseDTO = houseResponseDTOMono.block();


            Assertions.assertNotNull(houseResponseDTO);
            Assertions.assertNotNull(responseDTO.getId());
            Assertions.assertEquals(responseDTO.getName(), houseResponseDTO.getName());
            Assertions.assertEquals(responseDTO.getAddressHouseNumber(), houseResponseDTO.getAddressHouseNumber());
            Assertions.assertEquals(responseDTO.getAddressCity(), houseResponseDTO.getAddressCity());
            Assertions.assertEquals(responseDTO.getAddressAreaCode(), houseResponseDTO.getAddressAreaCode());
            Assertions.assertEquals(responseDTO.getAddressCountry(), houseResponseDTO.getAddressCountry());
        }

    }

    @Test
    public void updateHouseWhenHouseNotExists() {
        try (MockedStatic<UUID> utilities = Mockito.mockStatic(UUID.class)) {
            UUID uuid = Mockito.mock(UUID.class);
            utilities.when(UUID::randomUUID).thenReturn(uuid);

            ZonedDateTime zonedDateTime = ZonedDateTime.now();
            HouseEntity houseEntity = HouseEntity
                    .builder()
                    .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e")
                    .name("name")
                    .address(
                            Address
                                    .builder()
                                    .areaCode("areaCode")
                                    .city("city")
                                    .houseNumber("houseNumber")
                                    .country("country")
                                    .street("street")
                                    .build()
                    )
                    .createdAt(zonedDateTime)
                    .build();
            UpdateHouseRequestDTO houseResponseDTO = UpdateHouseRequestDTO
                    .builder()
                    .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e")
                    .name("name")
                    .addressAreaCode("areaCode")
                    .addressHouseNumber("houseNumber")
                    .addressCity("city")
                    .addressCountry("country")
                    .addressStreet("street")
                    .build();

            Mockito.when(housingRepository.getHouseById(houseResponseDTO.getId())).thenReturn(Mono.empty());
            Mono<HouseResponseDTO> houseResponseDTOMono = housingService.updateHouse(houseResponseDTO);
            StepVerifier.create(houseResponseDTOMono)
                    .expectError(HouseNotFoundException.class).verify();
        }
    }


    @Test
    public void updateHouseWhenHouseExists() {
        try (MockedStatic<UUID> utilities = Mockito.mockStatic(UUID.class)) {
            UUID uuid = Mockito.mock(UUID.class);
            Mockito.lenient().when(uuid.toString()).thenReturn("3d15133c-4a72-4b22-8a25-80364fa7fc5e");
            utilities.when(UUID::randomUUID).thenReturn(uuid);

            ZonedDateTime zonedDateTime = ZonedDateTime.now();
            HouseEntity houseEntity = HouseEntity
                    .builder()
                    .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e")
                    .name("name")
                    .address(
                            Address
                                    .builder()
                                    .areaCode("areaCode")
                                    .city("city")
                                    .houseNumber("houseNumber")
                                    .country("country")
                                    .street("street")
                                    .build()
                    )
                    .createdAt(zonedDateTime)
                    .updatedAt(zonedDateTime)
                    .build();
            UpdateHouseRequestDTO houseResponseDTO = UpdateHouseRequestDTO
                    .builder()
                    .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e")
                    .name("name")
                    .addressAreaCode("areaCode")
                    .addressHouseNumber("houseNumber")
                    .addressCity("city")
                    .addressCountry("country")
                    .addressStreet("street")
                    .build();

            Mockito.when(housingRepository.getHouseById(houseResponseDTO.getId())).thenReturn(Mono.just(houseEntity));
            Mockito.when(housingRepository.getHouseByName(houseResponseDTO.getName())).thenReturn(Mono.just(houseEntity));
            Mockito.when(housingRepository.updateHouse(houseEntity)).thenReturn(Mono.just(houseEntity));
            Mockito.when(dateUtil.utcDateFromCurrentDate()).thenReturn(zonedDateTime);
            Mono<HouseResponseDTO> houseResponseDTOMono = housingService.updateHouse(houseResponseDTO);
            HouseResponseDTO responseDTO = houseResponseDTOMono.block();


            Assertions.assertNotNull(houseResponseDTO);
            Assertions.assertNotNull(responseDTO.getId());
            Assertions.assertEquals(responseDTO.getName(), houseResponseDTO.getName());
            Assertions.assertEquals(responseDTO.getAddressHouseNumber(), houseResponseDTO.getAddressHouseNumber());
            Assertions.assertEquals(responseDTO.getAddressCity(), houseResponseDTO.getAddressCity());
            Assertions.assertEquals(responseDTO.getAddressAreaCode(), houseResponseDTO.getAddressAreaCode());
            Assertions.assertEquals(responseDTO.getAddressCountry(), houseResponseDTO.getAddressCountry());
        }
    }

    @Test
    public void updateHouseWhenHouseExistsWithNameOfAnotherHouse() {
        try (MockedStatic<UUID> utilities = Mockito.mockStatic(UUID.class)) {
            UUID uuid = Mockito.mock(UUID.class);
            Mockito.lenient().when(uuid.toString()).thenReturn("3d15133c-4a72-4b22-8a25-80364fa7fc5e");
            utilities.when(UUID::randomUUID).thenReturn(uuid);

            ZonedDateTime zonedDateTime = ZonedDateTime.now();
            HouseEntity houseEntity1 = HouseEntity
                    .builder()
                    .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e")
                    .name("name")
                    .address(
                            Address
                                    .builder()
                                    .areaCode("areaCode")
                                    .city("city")
                                    .houseNumber("houseNumber")
                                    .country("country")
                                    .street("street")
                                    .build()
                    )
                    .createdAt(zonedDateTime)
                    .updatedAt(zonedDateTime)
                    .build();

            HouseEntity houseEntity2 = HouseEntity
                    .builder()
                    .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e-1")
                    .name("name")
                    .address(
                            Address
                                    .builder()
                                    .areaCode("areaCode")
                                    .city("city")
                                    .houseNumber("houseNumber")
                                    .country("country")
                                    .street("street")
                                    .build()
                    )
                    .createdAt(zonedDateTime)
                    .updatedAt(zonedDateTime)
                    .build();
            UpdateHouseRequestDTO houseResponseDTO = UpdateHouseRequestDTO
                    .builder()
                    .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e")
                    .name("name")
                    .addressAreaCode("areaCode")
                    .addressHouseNumber("houseNumber")
                    .addressCity("city")
                    .addressCountry("country")
                    .addressStreet("street")
                    .build();

            Mockito.when(housingRepository.getHouseById(houseResponseDTO.getId())).thenReturn(Mono.just(houseEntity1));
            Mockito.when(housingRepository.getHouseByName(houseResponseDTO.getName())).thenReturn(Mono.just(houseEntity2));
            Mono<HouseResponseDTO> houseResponseDTOMono = housingService.updateHouse(houseResponseDTO);
            StepVerifier.create(houseResponseDTOMono)
                    .expectError(HouseAlreadyExistsException.class).verify();
        }
    }

    @Test
    public void deleteHouseWhenNotExists(){

        Mockito.when(housingRepository.getHouseById("id")).thenReturn(Mono.empty());
        Mono<Boolean> houseDeleteStatus = housingService.deleteHouseById("id");
        StepVerifier.create(houseDeleteStatus)
                .expectError(HouseNotFoundException.class).verify();
    }

    @Test
    public void deleteHouseWhenExists(){
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();
        Mockito.when(housingRepository.getHouseById("id")).thenReturn(Mono.just(houseEntity));
        Mockito.when(housingRepository.deleteHouseById("id")).thenReturn(Mono.just(Boolean.TRUE));
        Mono<Boolean> houseDeleteStatus = housingService.deleteHouseById("id");
        Boolean deleteStatus = houseDeleteStatus.block();
        Assertions.assertEquals(true,deleteStatus);
    }


    @Test
    public void deleteHouseWhenExistsButRepositoryError(){
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("3d15133c-4a72-4b22-8a25-80364fa7fc5e")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();
        Mockito.when(housingRepository.getHouseById("id")).thenReturn(Mono.just(houseEntity));
        Mockito.when(housingRepository.deleteHouseById("id")).thenReturn(Mono.just(Boolean.FALSE));
        Mono<Boolean> houseDeleteStatus = housingService.deleteHouseById("id");
        Boolean deleteStatus = houseDeleteStatus.block();
        Assertions.assertEquals(false,deleteStatus);
    }
}