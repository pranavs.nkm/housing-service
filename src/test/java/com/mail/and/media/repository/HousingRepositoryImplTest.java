package com.mail.and.media.repository;

import com.mail.and.media.entity.Address;
import com.mail.and.media.entity.HouseEntity;
import com.mail.and.media.store.HousingStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.ZonedDateTime;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class HousingRepositoryImplTest {
    private HousingRepository housingRepository;

    @Mock
    private HousingStore housingStore;

    @BeforeEach
    public void init() {
        this.housingRepository = new HousingRepositoryImpl(housingStore);
    }

    @Test
    public void addHouse() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .build();

        Mockito.when(housingStore.add(houseEntity)).thenReturn(Mono.just(houseEntity));

        HouseEntity houseEntityPersisted = housingRepository.addHouse(houseEntity).block();

        Assertions.assertEquals(houseEntity, houseEntityPersisted);
    }


    @Test
    public void updateHouse() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();

        Mockito.when(housingStore.update(houseEntity)).thenReturn(Mono.just(houseEntity));

        HouseEntity houseEntityPersisted = housingRepository.updateHouse(houseEntity).block();

        Assertions.assertEquals(houseEntity, houseEntityPersisted);
    }

    @Test
    public void deleteHouseWhenPresent() {
        Mockito.when(housingStore.delete("id")).thenReturn(Mono.just(Boolean.TRUE));
        Boolean houseEntityDeleted = housingRepository.deleteHouseById("id").block();
        Assertions.assertEquals(true, houseEntityDeleted);
    }

    @Test
    public void deleteHouseWhenAbsent() {
        Mockito.when(housingStore.delete("id")).thenReturn(Mono.just(Boolean.FALSE));
        Boolean houseEntityDeleted = housingRepository.deleteHouseById("id").block();
        Assertions.assertEquals(false, houseEntityDeleted);
    }

    @Test
    public void getAllHouses() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();

        Mockito.when(housingStore.get()).thenReturn(Flux.just(houseEntity));

        List<HouseEntity> houses = housingRepository.getAllHouses().collectList().block();

        Assertions.assertEquals(1, houses.size());
        Assertions.assertEquals(houseEntity, houses.get(0));
    }

    @Test
    public void getHouseByIdWhenExists() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();

        Mockito.when(housingStore.get("id")).thenReturn(Mono.just(houseEntity));

        HouseEntity house = housingRepository.getHouseById("id").block();

        Assertions.assertEquals(houseEntity, house);
    }

    @Test
    public void getHouseByIdWhenNotExists() {
        Mockito.when(housingStore.get("id")).thenReturn(Mono.empty());
        Mono<HouseEntity> house = housingRepository.getHouseById("id");
        StepVerifier.create(house).expectNextCount(0).verifyComplete();
    }


    @Test
    public void getHouseByNameWhenExists() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();

        Mockito.when(housingStore.getByName("name")).thenReturn(Mono.just(houseEntity));

        HouseEntity house = housingRepository.getHouseByName("name").block();

        Assertions.assertEquals(houseEntity, house);
    }

    @Test
    public void getHouseByNameWhenNotExists() {
        Mockito.when(housingStore.getByName("name")).thenReturn(Mono.empty());
        Mono<HouseEntity> house = housingRepository.getHouseByName("name");
        StepVerifier.create(house).expectNextCount(0).verifyComplete();
    }
}