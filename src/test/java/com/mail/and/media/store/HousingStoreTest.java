package com.mail.and.media.store;

import com.mail.and.media.entity.Address;
import com.mail.and.media.entity.HouseEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.ZonedDateTime;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class HousingStoreTest {

    private HousingStore housingStore;

    @BeforeEach
    public void init() {
        this.housingStore = new HousingStore();
    }

    @Test
    public void addHouse() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .build();

        HouseEntity houseEntityPersisted = housingStore.add(houseEntity).block();

        Assertions.assertEquals(houseEntity, houseEntityPersisted);
    }


    @Test
    public void updateHouse() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();

        HouseEntity houseEntityPersisted = housingStore.update(houseEntity).block();

        Assertions.assertEquals(houseEntity, houseEntityPersisted);
    }

    @Test
    public void deleteHouseWhenPresent() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();

        housingStore.add(houseEntity);
        Boolean houseEntityDeleted = housingStore.delete("id").block();
        Assertions.assertEquals(true, houseEntityDeleted);
    }

    @Test
    public void deleteHouseWhenAbsent() {
        Boolean houseEntityDeleted = housingStore.delete("id").block();
        Assertions.assertEquals(false, houseEntityDeleted);
    }

    @Test
    public void getAllHouses() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();

        housingStore.add(houseEntity);
        List<HouseEntity> houses = housingStore.get().collectList().block();

        Assertions.assertEquals(1, houses.size());
        Assertions.assertEquals(houseEntity, houses.get(0));
    }

    @Test
    public void getHouseByIdWhenExists() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();

        housingStore.add(houseEntity);
        HouseEntity house = housingStore.get("id").block();

        Assertions.assertEquals(houseEntity, house);
    }

    @Test
    public void getHouseByIdWhenNotExists() {
        Mono<HouseEntity> house = housingStore.get("id");
        StepVerifier.create(house).expectNextCount(0).verifyComplete();
    }


    @Test
    public void getHouseByNameWhenExists() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        HouseEntity houseEntity = HouseEntity
                .builder()
                .id("id")
                .name("name")
                .address(
                        Address
                                .builder()
                                .areaCode("areaCode")
                                .city("city")
                                .houseNumber("houseNumber")
                                .country("country")
                                .street("street")
                                .build()
                )
                .createdAt(zonedDateTime)
                .updatedAt(zonedDateTime)
                .build();


        housingStore.add(houseEntity);
        HouseEntity house = housingStore.getByName("name").block();

        Assertions.assertEquals(houseEntity, house);
    }

    @Test
    public void getHouseByNameWhenNotExists() {
        Mono<HouseEntity> house = housingStore.getByName("name");
        StepVerifier.create(house).expectNextCount(0).verifyComplete();
    }
}